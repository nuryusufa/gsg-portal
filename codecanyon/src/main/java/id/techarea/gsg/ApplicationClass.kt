package id.techarea.gsg

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.onesignal.OneSignal
import infix.imrankst1221.codecanyon.R
import id.techarea.gsg.ui.home.HomeActivity
import infix.imrankst1221.rocket.library.setting.AppDataInstance
import infix.imrankst1221.rocket.library.utility.Constants
import infix.imrankst1221.rocket.library.utility.PreferenceUtils
import infix.imrankst1221.rocket.library.utility.UtilMethods
import org.json.JSONException


class ApplicationClass: Application() {
    val TAG = "---ApplicationClass"
    lateinit var mContext: Context

    override fun onCreate() {
        super.onCreate()
        mContext = this

        initConfig()
        applyTheme()
    }

    private fun initConfig(){
        AppDataInstance.getINSTANCE(mContext)
        PreferenceUtils.getInstance().initPreferences(mContext)

        if(resources.getBoolean(R.bool.enable_firebase_notification)) {
            initFirebase()
        }
        if (resources.getBoolean(R.bool.enable_onesignal)) {
            initOnesignal()
        }
    }

    private fun initFirebase(){
        FirebaseApp.initializeApp(mContext)
        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(false)
            .build()
        FirebaseFirestore.getInstance().firestoreSettings = settings
    }

    private fun initOnesignal(){
        //OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.DEBUG)
        OneSignal.initWithContext(this)
        OneSignal.setAppId(getString(R.string.onesignal_app_id))

        OneSignal.setNotificationOpenedHandler { result ->
            try {
                val customKeyURL: String?
                val customKeyType: String?
                val actionType = result.action.type
                val notification = result.notification
                val launchURL = notification.launchURL
                val additionalData = notification.additionalData

                var notificationUrl: String = ""
                var notificationUrlOpenType: String = ""

                if (launchURL != null) {
                    notificationUrl = launchURL
                    notificationUrlOpenType = "INSIDE"
                    UtilMethods.printLog(TAG, "launchURL = $launchURL")
                }

                if (additionalData != null) {
                    customKeyURL = additionalData.optString(Constants.KEY_NOTIFICATION_URL, "")
                    if (customKeyURL.isNotEmpty()) {
                        customKeyType =
                            additionalData.optString(Constants.KEY_NOTIFICATION_OPEN_TYPE, "INSIDE")
                                .toUpperCase()
                        notificationUrl = customKeyURL
                        notificationUrlOpenType = customKeyType

                        UtilMethods.printLog(TAG, "customType = $customKeyType")
                        UtilMethods.printLog(TAG, "customURL = $customKeyURL")
                    }
                }

                if (HomeActivity::class.isInstance(mContext)) {
                    UtilMethods.printLog(TAG, "HomeActivity Instance.")
                    try {
                        AppDataInstance.notificationUrl = notificationUrl
                        AppDataInstance.notificationUrlOpenType = notificationUrlOpenType
                        (mContext as HomeActivity).notificationClickSync()
                    } catch (ex: Exception) {
                        UtilMethods.printLog(TAG, "" + ex.message)
                    }
                } else {
                    val intent = Intent(mContext.applicationContext, MainActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK

                    if (notificationUrl.isNotEmpty()) {
                        intent.putExtra(Constants.KEY_NOTIFICATION_URL, notificationUrl)
                        intent.putExtra(
                            Constants.KEY_NOTIFICATION_OPEN_TYPE,
                            notificationUrlOpenType
                        )
                    }
                    mContext.startActivity(intent)
                }
            }catch (ex: JSONException) {
                UtilMethods.printLog(TAG, ex.message.toString())
            }

        }

        /*OneSignal.startInit(this)
                .setNotificationOpenedHandler(OneSignalHandler(mContext))
                .autoPromptLocation(false) // true will be auto show location access when app run
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init()

        OneSignal.idsAvailable { userId, registrationId -> Log.d(TAG, ""+userId) }*/
    }
    private fun applyTheme(){
        /**
         * Turn on/off dark mode
         * Flag for off: AppCompatDelegate.MODE_NIGHT_NO
         * Flag for on: AppCompatDelegate.MODE_NIGHT_YES
         */
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        /**
         * Disable taking screenshot
         */
        //window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
    }
}
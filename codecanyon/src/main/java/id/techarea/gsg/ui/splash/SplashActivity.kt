package id.techarea.gsg.ui.splash

import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.core.content.ContextCompat
import android.view.View
import infix.imrankst1221.codecanyon.databinding.ActivitySplashBinding
import id.techarea.gsg.ui.home.HomeActivity
import infix.imrankst1221.rocket.library.setting.ThemeBaseActivity
import infix.imrankst1221.rocket.library.utility.PreferenceUtils
import infix.imrankst1221.rocket.library.utility.Constants
import infix.imrankst1221.rocket.library.utility.UtilMethods

class SplashActivity : ThemeBaseActivity() {
    lateinit var mBinding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mContext = this
        initView()
        gotoNextView()
    }

    private fun initView(){
        when(PreferenceUtils.getStringValue(Constants.KEY_SPLASH_SCREEN_TYPE, "")){
            Constants.SPLASH_SCREEN_TYPE.STANDER.name -> {
                mBinding.layoutWelcome.visibility = View.VISIBLE
                mBinding.imageFull.visibility = View.GONE
                mBinding.txtSplashQuote.text = PreferenceUtils.getStringValue(Constants.KEY_SPLASH_QUOTE, "")
                mBinding.txtSplashFooter.text = PreferenceUtils.getStringValue(Constants.KEY_SPLASH_FOOTER, "")
            }
            Constants.SPLASH_SCREEN_TYPE.FULL_SCREEN.name -> {
                mBinding.layoutWelcome.visibility = View.GONE
                mBinding.imageFull.visibility = View.VISIBLE
            }
        }
        val gradientDrawable = GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                intArrayOf(ContextCompat.getColor(mContext, UtilMethods.getThemePrimaryColor()),
                        ContextCompat.getColor(mContext,UtilMethods.getThemePrimaryDarkColor())))
        gradientDrawable.cornerRadius = 0f
        mBinding.viewBackground.background = gradientDrawable

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

    }

    private fun gotoNextView(){
        Handler(Looper.getMainLooper()).postDelayed({
            val intent = Intent(this, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        }, 3000)
    }

}
